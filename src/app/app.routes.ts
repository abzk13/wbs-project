import { Routes } from '@angular/router';
import {AppComponent} from "./app.component";
import {ImprintComponent} from "./components/imprint/imprint.component";
import {PpComponent} from "./components/pp/pp.component";
import {HomeComponent} from "./components/home/home.component";

export const routes: Routes = [
  { path: '', component: HomeComponent },

  {
  path: 'imprint', component:ImprintComponent },
  {path: 'pp', component:PpComponent}
];


