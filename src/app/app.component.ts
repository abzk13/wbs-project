import { Component } from '@angular/core';
import {RouterLink, RouterLinkActive, RouterModule, RouterOutlet} from '@angular/router';
import { TopNavbarComponent } from './components/top-navbar/top-navbar.component';
import {FooterComponent} from './components/footer/footer.component'
import { routes } from './app.routes';

function loadScripts() {
  const dynamicScripts = [
    'assets/js/wuerfel.js',
    'assets/js/cardShuffler.js',
    'assets/js/keno.js',
    'assets/js/lotto.js',
    'assets/js/bierdie.js',
  ];
  for (let i = 0; i < dynamicScripts.length; i++) {
    const node = document.createElement('script');
    node.src = dynamicScripts[i];
    node.type = 'text/javascript';
    node.async = false;
    document.getElementsByTagName('head')[0].appendChild(node);
  }
}

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, TopNavbarComponent, FooterComponent],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'wbsProjekt';

  ngAfterViewInit() {
    loadScripts();
  }
}
