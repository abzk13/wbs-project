import { Component } from '@angular/core';
import {TopNavbarComponent} from "../top-navbar/top-navbar.component";
import {RouterLink, RouterLinkActive} from "@angular/router";

@Component({
  selector: 'app-imprint',
  standalone: true,
  imports: [
    TopNavbarComponent,RouterLink, RouterLinkActive
  ],
  templateUrl: './imprint.component.html',
  styleUrl: './imprint.component.css'
})
export class ImprintComponent {

}
