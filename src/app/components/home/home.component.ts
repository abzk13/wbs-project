import { Component } from '@angular/core';
import {TopNavbarComponent} from "../top-navbar/top-navbar.component";

@Component({
  selector: 'app-home',
  standalone: true,
    imports: [
        TopNavbarComponent
    ],
  templateUrl: './home.component.html',
  styleUrl: './home.component.css'
})
export class HomeComponent {

}
