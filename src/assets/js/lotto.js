function toggleLottoContent() {
    const lottoTitle = document.getElementById('lottoTitle');
    const lottoContent = document.getElementById('lottoContent');


    lottoTitle.style.display = lottoTitle.style.display === 'none' ? 'block' : 'none';
    lottoContent.style.display = lottoContent.style.display === 'block' ? 'none' : 'block';
}

let lottoData = [];
let superzahlenData = [];

function generateLottoNumbers(anzahlTickets) {
    const requestData = {
        jsonrpc: '2.0',
        method: 'generateIntegers',
        params: {
            apiKey: apiKey,
            n: anzahlTickets * 6,
            min: 1,
            max: 49,
            replacement: false,
        },
        id: 42,
    };

    return fetch(apiUrl, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(requestData),
    })
        .then(response => response.json())
        .then(data => {
            if (data.result && data.result.random && data.result.random.data) {
                lottoData = data.result.random.data;
            } else {
                console.error('Unexpected response structure:', data);
                document.getElementById('lottoResults').innerText = 'Fehler beim Generieren der Zahlen.';
            }
        })
        .catch(error => {
            console.error('Error:', error);
            document.getElementById('lottoResults').innerText = 'Fehler beim Anfragen der Zahlen.';
        });
}

function generateSuperzahl(anzahlTickets) {
    const requestData = {
        jsonrpc: '2.0',
        method: 'generateIntegers',
        params: {
            apiKey: apiKey,
            n: anzahlTickets,
            min: 1,
            max: 9,
            replacement: false,
        },
        id: 42,
    };

    return fetch(apiUrl, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(requestData),
    })
        .then(response => response.json())
        .then(superzahlen => {
            if (superzahlen.result && superzahlen.result.random && superzahlen.result.random.data) {
                superzahlenData = superzahlen.result.random.data;
            } else {
                console.error('Unexpected response structure:', superzahlen);
                document.getElementById('lottoResults').innerText = 'Fehler beim Generieren der Zahlen.';
            }
        })
        .catch(error => {
            console.error('Error:', error);
            document.getElementById('lottoResults').innerText = 'Fehler beim Anfragen der Zahlen.';
        });
}

function lottotrain() {
    const anzahlTickets = document.getElementById('anzahlTickets').value;

    Promise.all([generateLottoNumbers(anzahlTickets), generateSuperzahl(anzahlTickets)])
        .then(() => {
            displayLottoResults(anzahlTickets);
        })
        .catch(error => {
            console.error('Error:', error);
            document.getElementById('lottoResults').innerText = 'Fehler beim Lottotrain.';
        });
}

function displayLottoResults(anzahlTickets) {
    const lottoResultsElement = document.getElementById('lottoResults');

    // Lösche alle vorhandenen Tickets
    lottoResultsElement.innerHTML = '';

    for (let i = 0; i < anzahlTickets; i++) {
        // Extrahiere 6 Zahlen für jedes Ticket
        const startIndex = i * 6;
        const ticket = lottoData.slice(startIndex, startIndex + 6);

        const superzahl = superzahlenData[i];

        // Überprüfe, ob das Ticket die richtige Anzahl von Zahlen hat
        if (ticket.length === 6) {
            const ticketHtml = `<p onclick="stopPropagation(event)">Ticket ${i + 1}: ${ticket.join('-')} / ${superzahl}</p>`;
            lottoResultsElement.innerHTML += ticketHtml;
        }
    }
}


