document.addEventListener("DOMContentLoaded", function() {
  document.getElementById("drawCard")?.addEventListener("click", drawCards);
  // Add other event listeners here
});

function toggleShuffleText(event) {
    const target = event.target;
    if (target.tagName !== 'INPUT' && target.tagName !== 'BUTTON') {
        var x = document.getElementById("shuffleText");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    }
}

function stopProp(event) {
    event.stopPropagation();
}

document.getElementById("drawCard").addEventListener("click", function () {
    var numCards = document.getElementById("numOfCards").value;
    var numDecks = document.getElementById("numOfDecks").value;
    const url = `https://www.random.org/playing-cards/?cards=${numCards}&decks=${numDecks}&spades=on&hearts=on&diamonds=on&clubs=on&aces=on&twos=on&threes=on&fours=on&fives=on&sixes=on&sevens=on&eights=on&nines=on&tens=on&jacks=on&queens=on&kings=on&remaining=on`;

    fetch(url)
        .then((response) => response.text())
        .then((html) => {
            const parser = new DOMParser();
            const doc = parser.parseFromString(html, "text/html");
            const pTags = doc.querySelectorAll("p");
            let outputText = "";

            pTags.forEach((p) => {
                const imgTags = p.querySelectorAll("img");
                imgTags.forEach((img, index) => {
                    const title = img.getAttribute('title');
                    if (title && title.includes('of')) {
                        outputText += `Card ${index + 1}: ${title}<br>`;
                    }
                });
            });

            // Output the text into the element with ID 'cardsOutput'
            document.getElementById("cardsOutput").innerHTML = outputText;
            console.log(outputText); // Correctly log the outputText without setting innerHTML again
        })
        .catch((error) => console.error("Error fetching or parsing:", error));
});
