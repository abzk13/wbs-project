function toggleShuffleText(event) {
    const target = event.target;
    if (target.tagName !== 'INPUT' && target.tagName !== 'BUTTON' && target.tagName !== 'SELECT') {
        var x = document.getElementById("shuffleText");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    }
}

function toggleBirdieText(event) {
    const target = event.target;
    if (target.tagName !== 'INPUT' && target.tagName !== 'BUTTON' && target.tagName !== 'SELECT') {
        var x = document.getElementById("birdieText");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    }
}

function drawCards(event) {
    event.stopPropagation();  // Prevent the card from toggling when the button is clicked
    const numCards = document.getElementById('numCards').value;
    const numDecks = document.getElementById('numDecks').value;

    const url = `https://www.random.org/playing-cards/?cards=${numCards}&decks=${numDecks}&spades=on&hearts=on&diamonds=on&clubs=on&aces=on&twos=on&threes=on&fours=on&fives=on&sixes=on&sevens=on&eights=on&nines=on&tens=on&jacks=on&queens=on&kings=on&format=plain`;

    fetch(url)
        .then(response => response.text())
        .then(data => {
            displayResult(data);
        })
        .catch(error => console.error('Error:', error));
}

function drawBirdies(event) {
    event.stopPropagation();  // Prevent the card from toggling when the button is clicked
    const numBirdies = document.getElementById('numBirdies').value;
    const courseType = document.getElementById('courseType').value;

    const url = `https://www.random.org/birdie-funds/?birdies=${numBirdies}&course=${courseType}&format=plain&rnd=new`;

    fetch(url)
        .then(response => response.text())
        .then(data => {
            displayBirdieResult(data);
        })
        .catch(error => console.error('Error:', error));
}

function displayResult(data) {
    const resultDiv = document.getElementById('result');
    resultDiv.innerHTML = '';

    const cards = data.split('\n').filter(line => line.trim() !== '');
    cards.forEach((card, index) => {
        const cardHtml = `<p onclick="stopPropagation(event)">Card ${index + 1}: ${card}</p>`;
        resultDiv.innerHTML += cardHtml;
    });
}

function displayBirdieResult(data) {
    const resultDiv = document.getElementById('birdieResult');
    resultDiv.innerHTML = '';

    const birdies = data.split('\n').filter(line => line.trim() !== '');
    birdies.forEach((birdie, index) => {
        const birdieHtml = `<p onclick="stopPropagation(event)">Birdie ${index + 1}: ${birdie}</p>`;
        resultDiv.innerHTML += birdieHtml;
    });
}

function stopPropagation(event) {
    event.stopPropagation();
}