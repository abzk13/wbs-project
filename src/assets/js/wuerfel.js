    const apiUrl = 'https://api.random.org/json-rpc/4/invoke';
    const apiKey = "35f70499-59e7-415f-9efb-fdb141fbd3a6";


function wuerfelWurf() {
    const anzahlWuerfel = document.getElementById('anzahlWuerfel').value;

   const requestData = {
        jsonrpc: '2.0',
        method: 'generateIntegers',
        params: {
            apiKey: apiKey,
            n: anzahlWuerfel,
            min: 1,
            max: 6,
            replacement: true
        },
        id: 42
    };

    fetch(apiUrl, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(requestData),
    })
    .then(response => response.json())
    .then(data => {
        if (data.result && data.result.random && data.result.random.data) {
            const wurfErgebnis = data.result.random.data.join(', ');
            document.getElementById('wurfErgebnis').innerText = 'Wurfergebnis: ' + wurfErgebnis;
        } else {
            console.error('Unexpected response structure:', data);
            document.getElementById('wurfErgebnis').innerText = 'Fehler beim Würfeln.';
        }
    })
    .catch(error => {
        console.error('Error:', error);
        document.getElementById('wurfErgebnis').innerText = 'Fehler beim Anfragen der Zufallszahl.';
    });
}





function toggleCoinText() {
        const coinText = document.getElementById('coinText');
        coinText.style.display = coinText.style.display === 'none' ? 'block' : 'none';
        if (coinText.style.display === 'block') {
            muenzwurf(); // Münzwurf wird automatisch ausgeführt, wenn sich die Karte dreht
        }
    }

    function muenzwurf() {
        const requestData = {
            jsonrpc: '2.0',
            method: 'generateIntegers',
            params: {
                apiKey: apiKey,
                n: 1,
                min: 0,
                max: 1,
                replacement: false,
            },
            id: 42,
        };

        fetch('https://api.random.org/json-rpc/4/invoke', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(requestData),
        })
            .then(response => response.json())
            .then(data => {
                console.log('Raw Response:', data);
                if (data.result && data.result.random && data.result.random.data) {
                    displayCoinResult(data.result.random.data[0]);
                } else {
                    console.error('Unexpected response structure:', data);
                    document.getElementById('coinResult').innerText = 'Error in coin toss.';
                }
            })
            .catch(error => {
                console.error('Error:', error);
                document.getElementById('coinResult').innerText = 'Error requesting coin toss.';
            });
    }

    function displayCoinResult(result) {
        const resultText = result === 0 ? 'Heads' : 'Tails';
        document.getElementById('coinResult').innerText = `Coin toss result: ${resultText}`;
    }

    function toggleCoinText() {
    const coinText = document.getElementById('coinText');
    const coinTitle = document.getElementById('coinTitle');
    coinText.style.display = coinText.style.display === 'none' ? 'block' : 'none';
    coinTitle.style.display = coinText.style.display === 'block' ? 'none' : 'block';

    if (coinText.style.display === 'block') {
        muenzwurf(); // Münzwurf wird automatisch ausgeführt, wenn sich die Karte dreht
    }
}







function toggleDiceText() {
        var diceText = document.getElementById('diceText');
        var diceTitle = document.getElementById('diceTitle');

        if (diceText.style.display === 'none' || diceText.style.display === '') {
            diceText.style.display = 'block';
            diceTitle.style.display = 'none';
        } else {
            diceText.style.display = 'none';
            diceTitle.style.display = 'block';
        }
    }

    function showDiceText() {
        var diceText = document.getElementById('diceText');
        var diceTitle = document.getElementById('diceTitle');

        diceText.style.display = 'block';
        diceTitle.style.display = 'none';
    }

     function stopPropagation(event) {
        event.stopPropagation();
    }