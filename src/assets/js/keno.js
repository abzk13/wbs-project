function toggleKenoContent() {
    const kenoTitle = document.getElementById('kenoTitle');
    const kenoContent = document.getElementById('kenoContent');

    kenoTitle.style.display = kenoTitle.style.display === 'none' ? 'block' : 'none';
    kenoContent.style.display = kenoContent.style.display === 'block' ? 'none' : 'block';
}


let kenoData = [];


function generateKenoNumbers(anzahlTickets, pool, anzahhlNummern) {
    const requestData = {
        jsonrpc: '2.0',
        method: 'generateIntegers',
        params: {
            apiKey: apiKey,
            n: anzahlTickets * anzahhlNummern,
            min: 1,
            max: pool,
            replacement: false,
        },
        id: 42,
    };

    return fetch(apiUrl, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(requestData),
    })
        .then(response => response.json())
        .then(data => {
            if (data.result && data.result.random && data.result.random.data) {
                kenoData = data.result.random.data;
            } else {
                console.error('Unexpected response structure:', data);
                document.getElementById('lottoResults').innerText = 'Fehler beim Generieren der Zahlen.';
            }
        })
        .catch(error => {
            console.error('Error:', error);
            document.getElementById('lottoResults').innerText = 'Fehler beim Anfragen der Zahlen.';
        });
}


function kenotrain() {
    const anzahlTickets = document.getElementById('anzahlkenoTickets').value;
    const anzahlNummern = document.getElementById('anzahlNummern').value;
    const pool = document.getElementById('pool').value;

    generateKenoNumbers(anzahlTickets, pool, anzahlNummern)
        .then(() => {
            displayKenoResults(anzahlTickets, anzahlNummern);
        })
        .catch(error => {
            console.error('Error:', error);
            document.getElementById('kenoResults').innerText = 'Fehler beim Keno-Training.';
        });
}


function displayKenoResults(anzahlTickets, anzahlNummern) {
    const kenoResultsElement = document.getElementById('kenoResults');

    // Lösche alle vorhandenen Tickets
    kenoResultsElement.innerHTML = '';

    for (let i = 0; i < anzahlTickets; i++) {
        const kenoticket = kenoData.slice(i * anzahlNummern, (i + 1) * anzahlNummern);


            const kenoticketHtml = `<p onclick="stopPropagation(event)">Ticket ${i + 1}: ${kenoticket.join('-')}</p>`;
            kenoResultsElement.innerHTML += kenoticketHtml;

    }
}

