# WbsProjekt

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 18.0.7.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Installation of the app

Click on the icon with the description 'Install wbsProject'
![Visuelle Darstellung des Icons](mdbilder/InstallIcon.png)

## Smartphone view

The smartphone view of the homepage :

![Index1](mdbilder/index1.png)
![Index2](mdbilder/index2.png)
![Index3](mdbilder/index3.png)


<details>
  <summary>The flipped cards on the homepage</summary>
  ![IndexCard1](mdbilder/card1.png)
  ![IndexCard1](mdbilder/card2.png)
</details>

<details>
  <summary>Private Policy</summary>
  ![IndexCard1](mdbilder/pp1.png)
  ![IndexCard1](mdbilder/pp2.png)
  ![IndexCard1](mdbilder/pp3.png)
</details>

<details>
  <summary>Imprint</summary>
  ![IndexCard1](mdbilder/im.png)
</details>


